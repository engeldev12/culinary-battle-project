module CulinaryBattle
	module Game

		class Battle
			attr_reader :status

			def self.start(first_plate:, second_plate:)
				new(first_plate: first_plate, second_plate: second_plate)
			end

			def points(plate)
				simb = plate.to_sym
				if simb == :first_plate
					return @first_plate.points
				elsif simb == :second_plate
					return @second_plate.points
				end
			end	

			private

			def initialize(first_plate:, second_plate:)
				@first_plate = first_plate
				@second_plate = second_plate
				@status = :started
			end

		end

		class Plate
			attr_reader :name, :player, :points

			def self.with(name:, player:)
				new(name: name, player: player)
			end

			private
			def initialize(name:, player:)
				@name = name
				@player = player
				@points = 0
			end
		end

	end
end