require "spec_helper"


module CulinaryBattle::Game

	RSpec.describe Battle do

		context 'cuando inicia la batalla' do
			
			let(:first_plate) { Plate.with name: "Pollo", player: "Engel" }
			let(:second_plate) { Plate.with name: "Arroz con Pollo", player: "Angel" }
			
			let(:battle) { Battle.start first_plate: first_plate, second_plate: second_plate }				

			it 'su status debería ser started' do
				expect(battle.status).to eq :started
			end

			it 'cada plato debería tener un puntaje de cero' do
				expect(battle.points(:first_plate)).to eq 0
				expect(battle.points(:second_plate)).to eq 0
			end	

		end

	end

end