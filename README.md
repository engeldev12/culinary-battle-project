# Culinary Battle Project

Este es un proyecto de prueba!

[![pipeline status](https://gitlab.com/engeldev12/culinary-battle-project/badges/master/pipeline.svg)](https://gitlab.com/engeldev12/culinary-battle-project/-/commits/master)  

[![coverage report](https://gitlab.com/engeldev12/culinary-battle-project/badges/master/coverage.svg)](https://gitlab.com/engeldev12/culinary-battle-project/-/commits/master)  

#Sobre la Battle

	* [ ] En cada batalla deberían participar dos platillos

	* [ ]	Cada platillo debería de tener:
			* [ ] Su nombre
			* [ ] Nombre del participante
			* [ ] Puntos obtenidos

	* [ ] En cada batalla se debería poder agregar los puntos a cada plato.

	* [ ] Un platillo debería tener 3 calificaciones.
	* [ ] Las calificaciones o puntaje deberían estar entre 0 - 20

	* [ ] Una batalla debería tener 3 estados
		* Iniciada : started
		* En proceso : in_process
		* Terminada : completed 

	* [ ] De cada batalla debería poder tener un ganador, en caso contrario un empate.

	* [ ] De cada batalla debería poder obtener el promedio de cada platillo.