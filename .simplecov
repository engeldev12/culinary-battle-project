SimpleCov.start do
  add_filter 'spec'
  add_filter 'lib/culinary_battle'
end